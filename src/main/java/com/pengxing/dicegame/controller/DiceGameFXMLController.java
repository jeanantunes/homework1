/**
 * Sample Skeleton for 'DiceGameFXML.fxml' Controller Class
 */

package com.pengxing.dicegame.controller;

import com.pengxing.dicegame.business.MessageBox;
import com.pengxing.dicegame.business.FormatChecker;
import com.pengxing.dicegame.data.Context;
import com.pengxing.dicegame.data.DiceBean;
import com.pengxing.dicegame.data.UserBean;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class DiceGameFXMLController {
    private UserBean userBean;//= Context.getInstance().currentUserBean();
    Stage prevStage;
    double betAmount = 0.0;
    FormatChecker formatchecker ;
    DiceBean dbean ;
    GamePanelFXMLController gController ;
    //private GamePanelFXMLController gamecontroller;


    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="welcome"
    private Label welcome; // Value injected by FXMLLoader

    @FXML // fx:id="usernametxt"
    private TextField usernametxt; // Value injected by FXMLLoader

    @FXML // fx:id="emailtext"
    private TextField emailtext; // Value injected by FXMLLoader

    @FXML // fx:id="regbtn"
    private Button regbtn; // Value injected by FXMLLoader

    @FXML // fx:id="startbtn"
    private Button startbtn; // Value injected by FXMLLoader

    @FXML // fx:id="exitbtn"
    private Button exitbtn; // Value injected by FXMLLoader

    @FXML // fx:id="amounttxt1"
    private TextField amounttxt1; // Value injected by FXMLLoader

    @FXML // fx:id="passwordField"
    private PasswordField passwordField; // Value injected by FXMLLoader

    @FXML
    void exitclick(ActionEvent event) {
          Platform.exit();
    }
//    public DiceBean DiceGameFXMLController(DiceBean db) { 
//           dbean = db;
//           dbean.setBalance(Double.parseDouble(amounttxt1.getText()));
//           dbean.setUserName(usernametxt.getText());
//           return dbean;
//   }
    
    double amount;
    String name ;
    @FXML
    void registerclick(ActionEvent event) throws SQLException {
             userBean.setGameJudge(true);
             userBean.setPlayAmount(Double.parseDouble(amounttxt1.getText()));
             amount =Double.parseDouble(amounttxt1.getText());
             System.out.println(userBean.getPlayAmount());
             userBean.setPlayerName(usernametxt.getText());
             name = usernametxt.getText();
             
             dbean.setBalance(Double.parseDouble(amounttxt1.getText()));
             dbean.setUserName(usernametxt.getText());
            
             if(formatchecker.usernameChecker(usernametxt.getText())==true&&
             formatchecker.passwordChecker(passwordField.getText())==true&&
             formatchecker.emailChecker(emailtext.getText())==true&&
             formatchecker.betAmountChecker(Double.parseDouble(amounttxt1.getText()))==true)
             {
                 
                 String msg = "Congratulations!! Game Is Sucessfully Registered.\n"
                    + "Start Game Now!!";
                 String title = "Registration Sucess!";
                 MessageBox.show(msg, title);
                 
             }        
            
    }
    
    public void setPrevStage(Stage stage){
         this.prevStage = stage;
    }
     public void promptInfo(){
        String usernameFormat= "1. Username cannot be empty.\n"
                + "2. No special letter.\n"
                + "3. Consist of numbers and letters \n"
                + "4. Less than 10 characters\n"
                + "5. Include 1 uppercase and lowercase letter";
        String passwordFormat = 
                  " 1. password cannot be empty.\n"
                + "2. Including the special letter.\n"
                + "3. Consist of numbers and letters \n"
                + "4. Less than 10 characters \n"
                + "5. Consist at least 1 uppercase and lowercase letter";
               ;
        String emailFormat =
                  "Please make sure your email address meet the following requirements: \n"
                 +"1. Email address cannot be empty.\n"
                + "2. No special letter.\n"
                + "3. Make sure it is a valid email";

        
        Tooltip tt = new Tooltip();
        tt.setText(usernameFormat);
        usernametxt.setTooltip(tt);
        
        Tooltip tt1 = new Tooltip();
        tt1.setText(passwordFormat);
        passwordField.setTooltip(tt1);
        
        Tooltip tt2 = new Tooltip();
        tt2.setText(emailFormat);
        emailtext.setTooltip(tt2);
    
    }
     public double passBetAmount(){
        return dbean.getBetAmount();
    }

    @FXML
    void startclick(ActionEvent event) throws IOException, SQLException {        
        if(userBean.getGameJudge()==true){
               Stage stage = new Stage();
               stage.setTitle("Dice Game Verson 1.0 Game Panel");
               Pane myPane = null;
               FXMLLoader myLoader = new FXMLLoader(getClass().getResource("/fxml/GamePanelFXML.fxml"));
               
               
               myPane = myLoader.load();               
               gController = (GamePanelFXMLController)(myLoader.getController());
               Scene scene = new Scene(myPane);
               stage.setScene(scene);
               prevStage.close();
               gController.setDiceBean(dbean);
               gController.setUserBean(userBean);
               stage.show();  
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert welcome != null : "fx:id=\"welcome\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert usernametxt != null : "fx:id=\"usernametxt\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert emailtext != null : "fx:id=\"emailtext\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert regbtn != null : "fx:id=\"regbtn\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert startbtn != null : "fx:id=\"startbtn\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert exitbtn != null : "fx:id=\"exitbtn\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert amounttxt1 != null : "fx:id=\"amounttxt1\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert passwordField != null : "fx:id=\"passwordField\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
         
    }
    
    
    public void setUserBean(UserBean userbean)throws SQLException {
        
        this.userBean = userbean;
        
    }
    
     public void setDiceBean(DiceBean dicebean)throws SQLException {
        this.dbean= dicebean;
        
    }
     
     
}
