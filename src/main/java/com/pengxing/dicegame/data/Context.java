/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.dicegame.data;

import com.pengxing.dicegame.data.UserBean;

/**
 *
 * @author leo
 */
public class Context {
    private final static Context instance = new Context();

    public static Context getInstance() {
        return instance;
    }

    private UserBean userbean = new UserBean();

    public UserBean currentUserBean() {
        return userbean;
    }

}
