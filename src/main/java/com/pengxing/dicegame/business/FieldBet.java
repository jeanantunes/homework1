/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.dicegame.business;

import com.pengxing.dicegame.data.DiceBean;

/**
 *
 * @author User
 */
public class FieldBet{
    DiceBean dicebean = new DiceBean();
    int total = 0;

    public void fieldBet(DiceBean db, int total){
         this.dicebean = db;
          switch(total){
             case 3: 
                  isWin(1);
                  break;
             case 4: 
                  isWin(1);
                  break;
             case 9:     
                  isWin(1);
                  break;
             case 10:
                  isWin(1);
                  break;
             case 11:
                  isWin(1);
                  break;
             case 2: 
                  isWin(2);
                  break; 
             case 12: 
                  isWin(3);
                  break;
             default:     
                  isLoose();
                  break;
          
          }
    
      
    }
    
    
    
     public void isWin(int n){

        dicebean.setBalance(n*dicebean.getBetAmount()+dicebean.getBalance());
        String msg = "You have win "+ n + "times of Your Bet:" + n*dicebean.getBetAmount()+"dollars";
        String title = "Congratulations!!";
        MessageBox.show(msg,title);
        
       
     }
     
     public void isLoose(){

        dicebean.setBalance(dicebean.getBalance()-dicebean.getBetAmount()); 
        String msg = "You have lose your bet: " + dicebean.getBetAmount()+" dollars"; 
        String title = "Sorry!!";
        MessageBox.show(msg,title);
        
       
     }
     
  

}
