/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.dicegame.business;

/**
 * This section is used to check if the user has inputed the right format information
 * @author User
 */
public class FormatChecker {
    
    public static boolean usernameChecker(String inputInfo){
         boolean checker = true;
         String errmsg =null;
         String title = "Username requirement";
         char ch = inputInfo.charAt(0);
               ;
        //check if the input info is empty
        if(inputInfo ==null||inputInfo.isEmpty()){    
           errmsg = "Username Can not be empty";
           MessageBox.show(errmsg, title);
           checker = false;
       
       //check if the input information contains illegal characters
       }else if(inputInfo.indexOf("~!@#$%^&*()<>/")>0){
            errmsg = "Please don't include illegal characters, such as:!,@,#,$,% and etc" ;
            MessageBox.show(errmsg, title);
            checker = false;
        
       //check if the input information exceed the length
       }else if(inputInfo.length()>10&&inputInfo.length()<5){
           errmsg = "Please make sure your input length is between 5 and 10 characters";
           MessageBox.show(errmsg, title);
           checker = false;
       
       // check lower case
       }else if(!Character.isLowerCase(ch)){
           boolean lowercase = true;
           for (int i=1; i<inputInfo.length(); i++)
                {
                   ch = inputInfo.charAt(i);
                   
                    if (!Character.isLowerCase(ch)){ 
                        lowercase =false;
                        
                   }
                }
           if(lowercase=false){
           errmsg="Invalid Username - Must have a Lower Case character.";    
           MessageBox.show(errmsg, title);
           checker = false;}
       
        //check upper case 
       }else if (!Character.isUpperCase(ch)){
                 boolean lowercase = true;
                for (int i=0; i<inputInfo.length(); i++)
                {

                    ch = inputInfo.charAt(i);

                    if (!Character.isUpperCase(ch))
                    {
                      lowercase = false;
                    }
                    }
                   if(lowercase=false){
                    errmsg="Invalid Username - Must have a Upper Case character.";    
                    MessageBox.show(errmsg, title);
                    checker = false;}
                }  
        return checker;
    }
    
    public static boolean passwordChecker(String inputInfo){
        boolean checker = true;
        String errmsg = null;
        
        String title="Password Setting";
        char ch = inputInfo.charAt(0);
       
        //check if the password field is empty
        if(inputInfo ==null||inputInfo.isEmpty()){     
           errmsg = "Password Can not be empty";
           MessageBox.show(errmsg, title);//when error happens this message window will show the rules
           checker = false;
        //check special letter
        }else if(inputInfo.indexOf("~!@#$%^&*()<>/")>=0){
           errmsg="Please include special character to your password settings";
           MessageBox.show(errmsg, title);
           checker = false;
           //check length
        }else if(inputInfo.length()>10&&inputInfo.length()<5){
           errmsg = "Please make sure your password length is between 5 and 10 characters";
           MessageBox.show(errmsg, title);
           checker = false;
           //check if the password contains lower case letters
        }else if(!Character.isLowerCase(ch)){
           boolean lowercase = true;
           for (int i=1; i<inputInfo.length(); i++)
                {
                   ch = inputInfo.charAt(i);
                   
                    if (!Character.isLowerCase(ch)){ 
                        lowercase =false;
                        
                   }
                }
           
           if(lowercase=false){
           errmsg="Invalid Password - Must have a Lower Case character.";    
           MessageBox.show(errmsg, title);
           checker = false;}
           
         //check upper case 
      
        }else if (!Character.isUpperCase(ch)){
                 boolean lowercase = true;
                for (int i=0; i<inputInfo.length(); i++)
                {

                    ch = inputInfo.charAt(i);

                    if (!Character.isUpperCase(ch))
                    {
                      lowercase =false;
                    }
                    }
                   if(lowercase=false){
                    errmsg="Invalid Password - Must have a Upper Case character.";    
                    MessageBox.show(errmsg, title);
                    checker = false;}
                }  
        return checker;
       }
    
     public static boolean emailChecker(String inputInfo){
        boolean checker = true;
        String errmsg = null;
        String title="Email Setting";
        
        //check if the password is empty
        if(inputInfo ==null||inputInfo.isEmpty()){     
           errmsg = "Password Can not be empty";
           MessageBox.show(errmsg, title);
           checker = false;
        
        //check if the email is valid format
        }else if(inputInfo.indexOf("@")<0){
             errmsg = "Invalid Email Format - Must contain @ in your email address";
             MessageBox.show(errmsg, title);
             checker = false;
       
        //check if the email address contains illegal letter
        }else if(inputInfo.indexOf("~!#$%^&*()<>/")>0){
           errmsg = "Invalid Email Format- don't include illegal characters, such as:!,#,$,% and etc" ;
            MessageBox.show(errmsg, title);
            checker = false;
        
        //check length if it is less than 20 letters
        }else if(inputInfo.length()>20&&inputInfo.length()<5){
           errmsg = "Please make sure your input length is between 5 and 20 characters";
           MessageBox.show(errmsg, title);
           checker = false;
        }
        return checker;
     }
     
      public static boolean betAmountChecker(double inputInfo){
          boolean checker = true;
          String errmsg = null;
           String title="Bet Amount Setting";
          if(inputInfo>200.0){
            errmsg = "Invalid Bet Amount - You cannot bet more than 200$ each time";
            MessageBox.show(errmsg, title);
            checker = false;
          }else if(inputInfo<10){
            errmsg ="Invalid Bet Amount - You cannot bet less than 10$ ";
            MessageBox.show(errmsg, title);
            checker = false;
          }
          return checker;
      }
}
